#include <time.h>
#include <stdio.h>
#include "cuda_utils.h"
#include "logging.h"

__global__ void kernel(float *x, int n)
{
  int index  = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  for (int i = index; i < n; i += stride ) {
      x[i] = i * i; 
  }
}


//{{{ init_memory
bool init_memory_managed( float* x, int N ){
  if ( check_cuda_error( cudaMallocManaged( &x, N*sizeof(float)))){
    return false;
  }
  return true;
}
//}}}


int main(){
  //LOG_LEVEL = LOG_DEBUG;
  INFO( "initializing\n");
  const int N = 102400000;

  // First set up fixed ping pong buffers 
  float *a, *d_a;
  check_cuda_error( cudaMallocHost((void**)&a, N*sizeof(float)));
  check_cuda_error( cudaMalloc((void**)&d_a, N*sizeof(float)));
  memset( a, 0, N*sizeof(float));
  INFO("Preparing %f MB to process\n", (double)N*sizeof(float)*1e-6);
  
  
  int block_size = 64;
  int num_blocks = ( N + block_size - 1 )/block_size;

  clock_t start = clock();
  cudaMemcpy(d_a, a, N*sizeof(float), cudaMemcpyHostToDevice);
  kernel<<<num_blocks, block_size  >>>(d_a, N);
  cudaMemcpy(a, d_a, N*sizeof(float), cudaMemcpyDeviceToHost);
  clock_t end = clock();
  double total_time = (double)( end - start )/ CLOCKS_PER_SEC;
  INFO( "Normal transfer = %f\n", total_time );
  // do a quick check of the results to make sure we didn't get fooled
  for (int k = 0; k<N; ++k ){
    if ( fabs( a[k]-k*k) >= 1.0e-9 ){
        ERROR( "Invalid = a[%d] = %f != %f\n",k, a[k], (float)k);
    }
  }

  // Now we will do this with multiple streams, 
  memset( a, 0, N*sizeof(float));
  // init the streams
  const int num_streams = 8;
  const int stream_size = N / num_streams;
  cudaStream_t streams[num_streams];
  for (int i = 0; i < num_streams; i++) {
    check_cuda_error( cudaStreamCreate(&streams[i]));
  }

  start = clock();
  for (int k = 0; k < num_streams; ++k ){
    int offset = k*stream_size;
    cudaMemcpyAsync(&d_a[offset], &a[offset], stream_size*sizeof(float),
                    cudaMemcpyHostToDevice,
                    streams[k] );
  }
  for (int k =0; k < num_streams; ++k ){
    kernel<<<stream_size/block_size, block_size,0,streams[k] >>>(d_a, N);
  }
  for (int k = 0; k < num_streams; ++k ){
    int offset = k*stream_size;
    cudaMemcpyAsync(&a[offset], &d_a[offset], stream_size*sizeof(float),
                    cudaMemcpyDeviceToHost,
                    streams[k] );
  }
  cudaDeviceSynchronize();
  end = clock();
  total_time = (double)( end - start )/ CLOCKS_PER_SEC;
  INFO( "Async transfer = %f\n", total_time );
  // do a quick check of the results to make sure we didn't get fooled

  for (int k = 0; k<N; ++k ){
    if ( fabs( a[k]-k*k) >= 1.0e-9 ){
        ERROR( "Invalid = a[%d] = %f != %f\n",k, a[k], (float)k);
    }
  }
// Now we will do it with managed memory same way, with / without streams


  float *a_m;
  cudaMallocManaged(&a_m, N*sizeof(float));
  memset( a_m, 0, N*sizeof(float));
  start = clock();
  kernel<<<num_blocks, block_size  >>>(a_m, N);
  cudaDeviceSynchronize();
  end = clock();
  total_time = (double)( end - start )/ CLOCKS_PER_SEC;
  INFO( "Managed no streams= %f\n", total_time );

  for (int k = 0; k<N; ++k ){
    if ( fabs( a[k]-k*k) >= 1.0e-9 ){
        ERROR( "Invalid = a[%d] = %f != %f\n",k, a_m[k], (float)k);
    }
  }

  memset( a_m, 0, N*sizeof(float));
  start = clock();
  for (int k =0; k < num_streams; ++k ){
    kernel<<<stream_size/block_size, block_size,0,streams[k] >>>(a_m, N);
  }
  cudaDeviceSynchronize();
  end = clock();
  total_time = (double)( end - start )/ CLOCKS_PER_SEC;
  INFO( "Managed streams= %f\n", total_time );

  for (int k = 0; k<N; ++k ){
    if ( fabs( a[k]-k*k) >= 1.0e-9 ){
        ERROR( "Invalid = a[%d] = %f != %f\n",k, a_m[k], (float)k);
    }
  }
//{{{ free the memory  
  cudaFreeHost(a);
  cudaFree(d_a);
  cudaFree(a_m);
  for (int i = 0; i < num_streams; i++) {
    check_cuda_error( cudaStreamDestroy( streams[i]));
  }

//}}}
  return 0;
}
