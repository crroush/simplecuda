#include <iostream>
#include <math.h>
#include <stdio.h>
#include <time.h>
// NVIDIA example just to ensure functionality
// Kernel function to add the elements of two arrays

__global__
void add(int n, float *x, float *y, float *z)
{
  int index  = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  for (int i = index; i < n; i+= stride )
    z[i] = x[i] + y[i];
}

void run_kernel( float* x, float* y, float* z, int N, int block_size, int num_blocks ){
  double avg_error  = 0.0;
  int num_trys      = 1000;
  double total_time = 0.0;

  for (int k = 0; k < num_trys; ++k ){
    // Do some timing of the kernel and how long it takes to go
    clock_t start = clock();
    add<<<num_blocks, block_size>>>(N, x, y, z);
    cudaDeviceSynchronize();
    clock_t end = clock();
    total_time += (double)( end - start )/ CLOCKS_PER_SEC;
    float max_error = 0.0f;
    float truth = 3.0f;
    for (int i = 0; i < N; i++){
      max_error = fmax(max_error, fabs(z[i]) - truth);
    }
    avg_error += max_error;
  }

  printf( "%d \t\t%d \t\t%f \t%f\n", block_size, num_blocks, avg_error, total_time );

}

int main(void)
{
  int N = 102400;
  float *x, *y, *z = NULL;

  // Allocate Unified Memory – accessible from CPU or GPU
  cudaMallocManaged(&x, N*sizeof(float));
  cudaMallocManaged(&y, N*sizeof(float));
  cudaMallocManaged(&z, N*sizeof(float));
  for (int i = 0; i < N; i++) {
    x[i] = 1.0f ;
    y[i] = 2.0f;
    z[i] = 0.0;
  }

  printf("blocksize \tnum_blocks \tavg_error \ttotal_time\n");
  // Run kernel on 1M elements on the GPU
  int block_size[9] = { 1, 2, 4, 8, 16, 32, 64, 128, 256};
  
  for (int k = 0; k < 9; ++k ){ 
    int num_blocks = ( N + block_size[k] - 1 )/block_size[k];
    run_kernel( x,y,z, N, block_size[k], num_blocks );
  }

  // Free memory
  cudaFree(x);
  cudaFree(y);
  cudaFree(z);
  
  return 0;
}

