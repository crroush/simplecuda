#ifndef LOGGING_H 
#define LOGGING_H 
// Stolen from John Segars and Adapted as a single header include

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <stdarg.h>

bool NO_PRINT = false ;
enum LOG_LEVELS{
  LOG_FATAL,
  LOG_ERROR,
  LOG_WARN,
  LOG_INFO,
  LOG_PRINT,
  LOG_DEBUG
};
LOG_LEVELS LOG_LEVEL = LOG_INFO;

void FmtMsg(FILE *stm,const char *file,int line,const char *func,
                const char *type,const char *fmt,...) {
   time_t t;
   struct tm *p;
   va_list ap;
   t=time(NULL);
   p=gmtime(&t);
   // Only allow up to N characters for the file
   // name to keep things aligned
   int max_fname=18;
   char fline[max_fname+1];
   // Initialize with spaces
   memset(fline,0x20,max_fname+1);
   char *fbase = basename((char *)file);
   int flen = strlen(fbase);
   if(flen > max_fname){
     // Trailing periods to indicate the file name was actually longer
     memcpy(fline,fbase,max_fname);
     memset(fline + (max_fname-3),0x2e,3);
   }
   else if(flen < max_fname){
     // Copy over the entire file name
     memcpy(fline,fbase,flen);
   }
   else strcpy(fline,fbase);
   // NULL terminate
   fline[max_fname] = '\0';

   if(!NO_PRINT ){
     fprintf(stm,"[%04d:%02d:%02d::%02d:%02d:%02d] %s(%04d)(%d) %s - ",
              p->tm_year+1900,
              p->tm_mon+1,
              p->tm_mday,
              p->tm_hour,
              p->tm_min,
              p->tm_sec,
              fline,line,(int)getpid(),type);
     va_start(ap,fmt);
     vfprintf(stm,fmt,ap);
     //fprintf(stm,"\n");
     va_end(ap);
   }
}

#define FATAL(fmt,...) if ( LOG_LEVEL >= 0 ) FmtMsg(stdout,__FILE__,__LINE__,__FUNCTION__,(isatty(fileno(stdout)) ? "[\033[1m\033[31m FATAL \033[0m]":"[ FATAL ]"),fmt,##__VA_ARGS__)
#define ERROR(fmt,...) if ( LOG_LEVEL >= 1 ) FmtMsg(stdout,__FILE__,__LINE__,__FUNCTION__,(isatty(fileno(stdout)) ? "[\033[1m\033[31m ERROR \033[0m]":"[ ERROR ]"),fmt,##__VA_ARGS__)
#define WARN(fmt,...)  if ( LOG_LEVEL >= 2 ) FmtMsg(stdout,__FILE__,__LINE__,__FUNCTION__,(isatty(fileno(stdout)) ? "[\033[35m WARN  \033[0m]":"[ WARN  ]"),fmt,##__VA_ARGS__)
#define INFO(fmt,...)  if ( LOG_LEVEL >= 3 ) FmtMsg(stdout,__FILE__,__LINE__,__FUNCTION__,(isatty(fileno(stdout)) ? "[\033[32m INFO  \033[0m]":"[ INFO  ]"),fmt,##__VA_ARGS__)
#define PRINT(fmt,...) if ( LOG_LEVEL >= 4 ) FmtMsg(stdout,__FILE__,__LINE__,__FUNCTION__,(isatty(fileno(stdout)) ? "[\033[32m PRINT \033[0m]":"[ PRINT ]"),fmt,##__VA_ARGS__)
#define DEBUG(fmt,...) if ( LOG_LEVEL >= 5 ) FmtMsg(stdout,__FILE__,__LINE__,__FUNCTION__,(isatty(fileno(stdout)) ? "[\033[34m DEBUG \033[0m]":"[ DEBUG ]"),fmt,##__VA_ARGS__)

#define FFATAL(stm,fmt,...) FmtMsg(stm,__FILE__,__LINE__,__FUNCTION__,"FATAL",fmt,##__VA_ARGS__)
#define FERROR(stm,fmt,...) FmtMsg(stm,__FILE__,__LINE__,__FUNCTION__,"ERROR",fmt,##__VA_ARGS__)
#define  FWARN(stm,fmt,...) FmtMsg(stm,__FILE__,__LINE__,__FUNCTION__,"WARN",fmt,##__VA_ARGS__)
#define  FINFO(stm,fmt,...) FmtMsg(stm,__FILE__,__LINE__,__FUNCTION__,"INFO",fmt,##__VA_ARGS__)
#define FPRINT(stm,fmt,...) FmtMsg(stm,__FILE__,__LINE__,__FUNCTION__,"PRINT",fmt,##__VA_ARGS__)
#define FDEBUG(stm,fmt,...) FmtMsg(stm,__FILE__,__LINE__,__FUNCTION__,"DEBUG",fmt,##__VA_ARGS__)

#endif
