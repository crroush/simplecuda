#pragma once
#include <stdio.h>
#include "logging.h"


static inline bool check_cuda_error( cudaError_t r ){
  if (r != cudaSuccess ){
    ERROR(  "Cuda Failed with: %s\n", cudaGetErrorString(r)); 
    return false;
  }
    DEBUG(  "Cuda Passed with: %s\n", cudaGetErrorString(r)); 
  return true; 
}
//{{{ void print_device_prop(cudaDeviceProp prop)
void print_device_prop(cudaDeviceProp prop){
  INFO("Name: \t\t\t%s\n",prop.name);
  INFO("Compute Capability: \t%d.%d\n",prop.major,prop.minor);
  INFO("Integrated: \t\t%s\n",(prop.integrated == 1 ? "\033[92mYes\033[0m":"\033[91mNo\033[0m"));
  INFO("Can map host mem: \t%s\n",(prop.canMapHostMemory == 1 ? "\033[92mYes\033[0m":"\033[91mNo\033[0m"));
  INFO("Clock rate: \t\t%d\n",prop.clockRate);
  INFO("Memory clock rate: \t%d\n",prop.memoryClockRate);
  INFO("Concurrent Kernels: \t%s\n",(prop.concurrentKernels == 1 ? "\033[92mYes\033[0m":"\033[91mNo\033[0m"));
  INFO("MP Count: \t\t%d\n",prop.multiProcessorCount);
  INFO("Max threads per MP: \t%d\n",prop.maxThreadsPerMultiProcessor);
  INFO("Max threads per block: \t%d\n",prop.maxThreadsPerBlock);
  INFO("Total const mem: \t%d\n",prop.totalConstMem);
  INFO("Total global mem: \t%u\n",prop.totalGlobalMem);
  INFO("Warp size: \t\t%d\n",prop.warpSize);
  INFO("Async engine count: \t%d\n",prop.asyncEngineCount);
  INFO("Max grid size x: \t%d\n",prop.maxGridSize[0]);
  INFO("Max grid size y: \t%d\n",prop.maxGridSize[1]);
  INFO("Max grid size z: \t%d\n",prop.maxGridSize[2]);
  INFO("Max threads x: \t\t%d\n",prop.maxThreadsDim[0]);
  INFO("Max threads y: \t\t%d\n",prop.maxThreadsDim[1]);

  INFO("Max threads z: \t\t%d\n",prop.maxThreadsDim[2]);
}
//}}}

//{{{ void show_devices()
void show_devices(){
  cudaDeviceProp prop;

  int count;
  cudaError cuda_error;

  cuda_error = cudaGetDeviceCount(&count);

  if(cuda_error == cudaSuccess){
    INFO("There are %d devices available\n",count);
    for(int i=0;i<count;i++){
      cuda_error = cudaGetDeviceProperties(&prop,i);
      INFO("------------ Device %d ------------\n",i);
      print_device_prop(prop);
    }
  } else ERROR("An error occurred while attempting to retrieve available devices: %s\n",cudaGetErrorString(cuda_error));
}
//}}}

// {{{ int get_device(cudaDeviceProp *p, int dev_num)
int get_device(cudaDeviceProp *p, int dev_num){
  int device_found=-1;
  cudaError_t err;

  if(p == NULL) return device_found;

  if(dev_num > -1){
    // If a specific device was requested then assume the user knows
    // what they're doing and blindly set the device and grab the
    // properties.
    err = cudaSetDevice(dev_num);
    if(err == cudaSuccess){
      err = cudaGetDeviceProperties(p,dev_num);
      check_cuda_error(err);
    } else check_cuda_error(err);
  }
  else {
    // Check for a device that can meet the libraries maximum requirements
    p->major=3;
    p->minor=0;

    err = cudaChooseDevice(&device_found,p);
    
    if(err == cudaSuccess){
      err = cudaSetDevice(device_found);
      check_cuda_error(err);
      err = cudaGetDeviceProperties(p,device_found);
      check_cuda_error(err);
    } else check_cuda_error(err);
  }

  if(err == cudaSuccess){
    INFO("------------ Selected Device ------------\n");
    print_device_prop(*p);
  }

  return device_found;
}
//}}}
