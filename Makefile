################################################################################
# This is a general-purpose Makefile for building C/C++ projects.  
# 
# Some features contained within:
#   * automatic dependency tracking, header files are parsed out of source 
#     and targets generated so that when they change, only the needed binaries
#     are re-compiled.
#   * distribution tarball generation (make dist)
#   * header file verification (make headercheck)
################################################################################

# compiler configuration
override CC      := g++
override NVCC    := nvcc
override CCOPTS  := -Wall -Wextra -Iinc/ -O3 $(CCOPTS)
override NVCCOPTS  := --default-stream per-thread  -D_FORCE_INLINES -Iinc/ 
#override LDOPTS  := $(LDOPTS) -lpthread

# linux requires -lrt, OSX does not
UNAME := $(shell uname -s)
#ifeq ($(UNAME),Linux)
#    override LDOPTS += -lrt
#endif

# define project name and help.  This will tell us the tarball to generate
# and provide help text to print when "make help" is run
PROJECT_NAME := simplecuda 
PROJECT_HELP := "Help text goes here, will be followed by target documentation"

# define source code to compile here
PROGRAMS := \
	bin/simplecuda \
	bin/simplemem


################################################################################
# Dependency generation stuff
################################################################################
DEPDIR := .deps

define pad 
$(shell echo $1 | sed -e :a -e 's/^.\{1,$2\}$$/& /;ta')
endef

# Make dependency directory
$(DEPDIR):
	@mkdir -p $(DEPDIR)

# and object directory
$(OBJDIR):
	@mkdir -p $(OBJDIR)

# Function to create dependency information using C-preprocessor
# Looks for lines of the form # 1 "inc/verify.h" 1 from pre-preprocessor
DEPFILE    = $(DEPDIR)/$(@F).d
MAKEDEPEND = $(CC) -E $(CCOPTS) $(1) $< | sed -n 's/^\# *[0-9][0-9]* *"\([^<"]*\)".*/bin\/$(@F): \1/p'  | sort | uniq > $(DEPFILE)
MAKEDEPEND_CUDA = $(NVCC) -E $(NVCCOPTS) $(1) $< | sed -n 's/^\# *[0-9][0-9]* *"\([^<"]*\)".*/bin\/$(@F): \1/p'  | sort | uniq > $(DEPFILE)

# This goes line by line in the .d file and generates an empty make target
# for each dependency.  Then, if the dependency moves or changes names, make
# will just regenerate them instead of throwing a "no such target" error.
MAKEDUMMY  = cp $(DEPDIR)/$(@F).d $(DEPDIR)/$(@F).P; \
	sed -e 's/\#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	    -e '/^$$/ d'  -e 's/$$/ :/' < $(DEPDIR)/$(@F).d >> $(DEPDIR)/$(@F).P; \
	rm -f $(DEPDIR)/$(@F).d	


################################################################################
# Define targets
################################################################################
all: $(DEPDIR) $(PROGRAMS)    ## (default target) build everything

debug: CCOPTS += -ggdb3 -O0 
debug: all

# General binary target for C++
bin/% : src/%.cc Makefile
	@mkdir -p bin
	@$(MAKEDEPEND)	         
	@$(MAKEDUMMY)
	@echo Building $@
ifndef VERBOSE
	@$(CC) $(CCOPTS) $< -o $@ $(LDOPTS)
else
	$(CC) $(CCOPTS) $< -o $@ $(LDOPTS)
endif

# General binary target for C
bin/% : src/%.c Makefile
	@mkdir -p bin
	@$(MAKEDEPEND)	         
	@$(MAKEDUMMY)
	@echo Building $@
ifndef VERBOSE
	@$(CC) -std=c99 $(CCOPTS) $< -o $@ $(LDOPTS)
else
	$(CC) -std=c99 $(CCOPTS) $< -o $@ $(LDOPTS)
endif

# General binary target for C
bin/% : src/%.cu Makefile
	@mkdir -p bin
	@$(MAKEDEPEND_CUDA)	         
	@$(MAKEDUMMY)
	@echo Building $@
ifndef VERBOSE
	@$(NVCC)  $(NVCCOPTS) $< -o $@ $(LDOPTS)
else
	$(NVCC)  $(NVCCOPTS) $< -o $@ $(LDOPTS)
endif


dist: $(PROJECT_NAME).tar.bz2  ## build project distribution tarball


$(PROJECT_NAME).tar.bz2: all
	@echo Building $(PROJECT_NAME).tar.bz2 for distribution
	@rm -f $(PROJECT_NAME).tar.bz2
	@rm -rf /tmp/$(PROJECT_NAME)
	@cp -r ../$(PROJECT_NAME) /tmp
	@cd /tmp/; make -C $(PROJECT_NAME) realclean; rm -rf $(PROJECT_NAME)/.git; rm -rf $(PROJECT_NAME)/.objs; tar -cjf $(PROJECT_NAME).tar.bz2 $(PROJECT_NAME)
	@mv /tmp/$(PROJECT_NAME).tar.bz2 ./
	@rm -rf /tmp/$(PROJECT_NAME)


depclean:                      ## remove dependency info
	@rm -f $(DEPDIR)/*


clean:                         ## clean normal build detritus, but not dependency info
	@rm -f $(PROGRAMS)
	@rm -f $(LIBRARIES)
	@rm -f $(MOCFILES)


realclean: clean depclean      ## clean everything and remove depenency directory
	@rm -rf $(DEPDIR)


remake: clean all


# headercheck attempts to compile each header file as a standalone binary
# this is useful for ensuring consistency, and that each headerfile properly
# includes all it's required dependencies so it can easily be sliced out.
HEADERSRC = $(wildcard inc/*.h)
HEADEROBJ = $(patsubst %.h, %h.o, $(HEADERSRC))

%.h.c: %.h
	@cp $< $@

%.h.o: %.h.c
	$(CC) $(CCOPTS) -c $< -o $@
	@rm $@ $<

.PHONY: headercheck
headercheck: $(HEADEROBJ)      ## run header check to verify consistency and isolation of headers


# print help string 
COLWIDTH = 15

.PHONY: help
help:                          ## print this message and exit
	@echo $(PROJECT_HELP)
	@echo
	@echo "Useful command-line variables:"
	@echo "  VERBOSE=1 -- enable verbose mode, print compilation commands"
	@echo
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:[[:space:]]*.*?## / {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | sort


# Include generated dependencies, Make will re-parse these and do the right thing
-include $(patsubst bin/%,$(DEPDIR)/%.P,$(PROGRAMS))
-include $(patsubst lib/%,$(DEPDIR)/%.P,$(LIBRARIES))
